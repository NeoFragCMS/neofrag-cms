# NeoFrag CMS

[![Tag](http://img.shields.io/github/tag/NeoFragCMS/neofrag-cms.svg)](https://github.com/NeoFragCMS/neofrag-cms/tags)

NeoFrag est un CMS qui vous offre des fonctionnalités évoluées pour la gestion de votre équipe de jeu, association sportive ou esportive ! Facile à prendre en main, complet et performant, vous pouvez créer votre site internet avec des interfaces modernes et personnalisables, le tout gratuitement, sans connaissance particulière.

##### En savoir plus sur NeoFrag CMS

- [Présentation](#présentation)
- [Prérequis](#prérequis)
- [Installation](#fr-installation)
- [Contribuer](#contribuer)

## Présentation

NeoFrag CMS est un projet imaginé en 2011, qui a vu ça première version Alpha 0.1 lancée en juin 2015. Pour en savoir plus et rester informé de l'avancement du projet, rendez-nous visite sur le site officiel [NeoFrag CMS](http://www.neofrag.fr).

- Sa rapidité d'exécution permet le chargement des pages en un éclair ! NeoFrag est un CMS petit, mais puissant.
- De nombreux contenus additionnels seront disponibles sur le site officiel [NeoFrag CMS](http://www.neofrag.fr), thèmes graphiques, modules et widgets pourront être installés automatiquement via l'interface de gestion de votre site.
- L'architecture innovante de NeoFrag CMS vous permettra de mettre à jour automatiquement et simplement votre site. Gestion évoluée des permissions des utilisateurs via des groupes.
- Une documentation technique et de nombreux tutoriels vidéo seront mis à disposition pour que les développeurs chevronnés ainsi que les utilisateurs débutants puissent prendre en main aisément NeoFrag CMS.
- Bénéficiez d'un outil intuitif vous permettant de changer le contenu et la disposition des pages en direct sur votre site. Déplacez une rubrique d'un simple click et appréciez le résultat en live !
- Des interfaces adaptées pour tout type de support afin de vous offrir une accessibilité optimale à vos contenus. Consultez l'activité de votre site depuis votre tablette ou smartphone ! 

## Prérequis

NeoFrag CMS nécessite un hébergement web Apache / PHP / MySQL pour fonctionner.

- PHP 5.4 ou supérieur (avec les extensions gd2, json, mbstring et zip)
- Module Apache rewrite_module activé

:warning: Les versions de PHP supérieures à 5.4 n'ont pas encore pu être toutes testées.

## :fr: Installation

- Copier tous les fichiers et les dossiers de l'archive sur vote site web via FTP
- Créer une base de données MySQL et importer le fichier DATABASE.sql
- Éditer le fichier de configuration de la base de données ./config/database.php
- Connectez-vous sur votre site web avec comme identifiant: admin et mot de passe: admin123
- Profitez de [NeoFrag CMS](http://www.neofrag.fr) Alpha 0.1.2 !

## :uk: Installation

- Copy all files and directories on website by FTP
- Create a MySQL database and import DATABASE.sql
- Edit database config file in ./config/database.php
- Connect on your website and login with username: admin and password: admin123
- Let's enjoy [NeoFrag CMS](http://www.neofrag.fr) Alpha 0.1.2

## Contribuer

Si ce projet vous intéresse et que vous souhaitez nous aider, rendez-nous visite sur le site [NeoFrag CMS](http://www.neofrag.fr) pour en apprendre d'avantage sur le fonctionnement du CMS.

- Signalez-nous les bugs que vous remarquez sur [notre forum](http://www.neofrag.fr) ou via [Github](https://github.com/NeoFragCMS/neofrag-cms/issues).
- Suivez [@NeoFragCMS](https://twitter.com/NeoFragCMS) sur Twitter

Copyright © 2015 Michaël BILCOT & Jérémy VALENTIN