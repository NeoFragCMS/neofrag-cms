<?php if (!defined('NEOFRAG_CMS')) exit;
/**************************************************************************
Copyright © 2015 Michaël BILCOT & Jérémy VALENTIN

This file is part of NeoFrag.

NeoFrag is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

NeoFrag is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with NeoFrag. If not, see <http://www.gnu.org/licenses/>.
**************************************************************************/

$lang = array(
	'error'            => 'Error',
	'unfound'          => 'Page not found',
	'unauthorized'     => 'Acces deny',
	'unconnected'      => 'Authorization required',
	'database'         => 'Database connection error',
	'page_unfound'     => 'La page que vous souhaitez consulter est introuvable.',
	'page_unconnected' => 'La page que vous souhaitez consulter n\'est accessible qu\'aux utilisateurs connectés.<br />
		<br />
		Connectez-vous si vous avez déjà un compte utilisateur.<br />
		Vous pouvez aussi créé un nouveau compte en vous inscrivant ci-dessous.'
);

/*
NeoFrag Alpha 0.1
./neofrag/modules/error/lang/en.php
*/